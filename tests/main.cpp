#include<iostream>
#include<stdio.h>
#include"range.hpp"
#include"main.h"
using namespace std;
void TestRange(const char *inFile, const char *outFile){
  cout<<"file to compress: "<<inFile<<"; compressed file: "<<outFile;
  cout<<"; decompressed file: result"<<endl;
  Range Compress(inFile, outFile);
  Compress.compress();
  cout<<"successful compression!"<<endl;
  Compress.closeFile();
  Range Decompress(outFile, "../tests/result");
  cout<<"successful decompression!"<<endl;
  Decompress.decompress();
  Decompress.closeFile();
  FILE *in = fopen(inFile, "r");
  FILE *out = fopen(outFile, "r");
  FILE *res = fopen("../tests/result", "r");
  if (!in || !out || !res){
    throw "cant open file(s)";
  }
  fseek(in, 0, 2);
  fseek(out, 0, 2);
  fseek(res, 0, 2);
  long int x = ftell(in);
  long int y = ftell(out);
  long int z = ftell(res) + 1;
  cout<<"size of input file: "<<x<<endl;
  cout<<"size of compressed file: "<<y<<endl;
  cout<<"size of decompressed file: "<<z<<endl;
  cout<<"coef: "<<static_cast<double>(y)/static_cast<double>(x)<<endl;
}
int main(){
  try{
    TestRange("../tests/input", "../tests/outtxt");
    cout<<endl;
    TestRange("../tests/a.out", "../tests/outexec");
    cout<<endl;
    TestRange("../tests/range.cpp", "../tests/outsrc");
    cout<<endl;
    TestRange("../tests/alot", "../tests/outalot");
  }
  catch(const char *x){
    cout<<x<<endl;
  }
  catch(...){
    cout<<"smth wrong"<<endl;
  }
}
