#include"range.h"

using namespace std;

symFreq::symFreq(int k){
  freq = k;
}

symFreq::symFreq(){
  freq = 0;
  prevFreq = 0;
}

Range::Range(){
  in = NULL;
  out = NULL;
  countSym = 0;
}

Range::Range(const char *inFile, const char *outFile){
  countSym = 0;
  inputFile = inFile;
  outputFile = outFile;
  if ((in = fopen(inFile, "r")) == NULL){
    throw "cant open input file";
  }
  if ((out = fopen(outFile, "w")) == NULL){
    throw "cant open output file";
  }
}

Range::~Range(){
  if (in != NULL){
    fclose(in);
  }
  if (out != NULL){
    fclose(out);
  }
}

void Range::saveTable(){
  fwrite(&countSym, sizeof(int), 1, out);
  for (auto it = tabSym.begin(); it != tabSym.end(); ++it){
    fwrite(&(it->first), sizeof(short), 1, out);
    fwrite(&(it->second->freq), sizeof(int), 1, out);
  }
  short x = 1000;
  fwrite(&x, sizeof(short), 1, out);
}

void Range::loadTable(){
  fread(&countSym, sizeof(int), 1, in);
  short x;
  int k, sum = 0;
  while (1){
    fread(&x, sizeof(short), 1, in);
    if (x == 1000){
      break;
    }
    fread(&k, sizeof(int), 1, in);
    tabSym.insert(make_pair(x, new symFreq(k)));
    sum++;
  }
  sum = 0;
  for (auto it = tabSym.begin(); it != tabSym.end(); ++it){
    it->second->prevFreq = sum;
    sum += it->second->freq;
  }
}

void Range::compress(){
  checkClass();
  low = 0;
  range = TOP;
  readFile();
  saveTable();
  fseek(in, 0, 0);
  char x = 0;
  int k = tabSym.size();
  while (k != 0){
    k >>= 1;
    x++;
  }
  bottom = TOP >> (56 - x);
  while (countSym != k){
    fread(&x, sizeof(char), 1, in);
    it = tabSym.find(x);
    encode(it->second->freq, it->second->prevFreq, countSym);
    k++;
  }
  offset.push_back(low + range/4UL);
  for (auto itV = offset.begin(); itV != offset.end(); ++itV){
    uint64_t y = *itV;
    fwrite(&y, sizeof(uint64_t), 1, out);
  }
}
int Range::log2(int x){
  int res = 0;
  while (x != 0){
    x >>= 1;
    res++;
  }
  return res;
}
void Range::decompress(){
  low = 0;
  char x;
  int k;
  prevLow = low;
  checkClass();
  range = TOP;
  loadTable();
  bottom = TOP >> (56 - log2(tabSym.size()));
  fread(&cur, sizeof(uint64_t), 1, in);
  for (int i = 0; i<countSym; i++){
    x = decode();
    fwrite(&x, sizeof(char), 1, out);
  }
  cout<<endl;
}
char Range::decode(){
  range /= countSym;
  uint64_t r = range;
  for (auto it = tabSym.begin(); it != tabSym.end(); ++it){
    low = prevLow + r*it->second->prevFreq;
    range = r*it->second->freq;
    if ((low <= cur) && (cur <= low + range)){
      prevLow = low;
      decodeNormalize();
      return it->first;
    }
  }
  throw "cant decode symbol";
}

void Range::encode(int freq, int prevFreq, int countSym){
  range /= countSym;
  low += range*prevFreq;
  range *= freq;
  encodeNormalize();
}

void Range::encodeNormalize(){
  if (range < bottom){
    offset.push_back(low+range/4UL);
    range = TOP;
    low = 0;
  }
}

void Range::decodeNormalize(){
  if (range < bottom){
    range = TOP;
    low = 0;
    prevLow = 0;
    fread(&cur, sizeof(uint64_t), 1, in);
  }
}

void Range::setInFile(const char *nameFile){
  if ((in = fopen(nameFile, "r")) == NULL){
    throw "cant open file";
  }
}
void Range::setOutFile(const char *nameFile){
  if ((out = fopen(nameFile, "w")) == NULL){
    throw "cant open file";
  }
}
void Range::readFile(){
  char x;
  fseek(in, 0, 2);
  int length = ftell(in);
  fseek(in, 0, 0);
  while (fread(&x, sizeof(char), 1, in) == 1){
    if (!tabSym.count(x)){
      tabSym.insert(make_pair(x, new symFreq()));
    }
    it = tabSym.find(x);
    it->second->freq++;
    countSym++;
    if (countSym == length-1){
      break;
    }
  }
  int k = 0;
  for (auto it = tabSym.begin(); it != tabSym.end(); ++it){
    it->second->prevFreq=k;
    k += it->second->freq;
  }
}

void Range::checkClass(){
  if (in == NULL){
    throw "set an input file!";
  }
  if (out == NULL){
    throw "set an output file!";
  }
}

int main(int argc, char *argv[]){
  if ((argc != 2)||((argv[1][0] != 'e')&&(argv[1][0] != 'd'))){
    cout<<"usage: exec {e,d}"<<endl<<"where e - compress, d - decompress"<<endl;
    return -1;
  }
  try{
    if (argv[1][0] == 'e'){
      Range A("range.cpp","output");
      A.compress();
    }
    if (argv[1][0] == 'd'){
      Range A("output", "result");
      A.decompress();
    }
  }
  catch(string x){
    cout<<x<<endl;
  }
  catch(const char *x){
    cout<<x<<endl;
  }
  catch(...){
    cout<<"smth wrong"<<endl;
  }
}
