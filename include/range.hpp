#ifndef RANGE_HPP
#define RANGE_HPP
#include<iostream>
#include<fstream>
#include<string>
#include<stdint.h>
#include<stdio.h>
#include<stdlib.h>
#include<map>
#include<vector>
#define TOP (1UL<<63)
#define BOTTOM (TOP>>47)
#define BIGBYTE (0xFFUL<<(48))
typedef unsigned char uchar;
using namespace std;
struct symFreq{
  int freq;
  int prevFreq;
  symFreq(int k);
  symFreq();
};
class Range{
  map<uchar, symFreq*> tabSym;
  map<uchar, symFreq*>::iterator it;
  vector<uint64_t> offset;
  vector<uint64_t>::iterator itV;
  FILE *in;
  FILE *out;
  string outputFile;
  string inputFile;
  uint64_t bottom, low, prevLow, cur, range;
  int countSym;
  int log2(long unsigned int x);
  void saveTable();
  void loadTable();
  char decode();
  void encode(int freq, int prevFreq, int countSym);
  void encodeNormalize();
  void decodeNormalize();
  void readFile();
  void checkClass();
 public:
  Range& operator=(const Range& right);
  Range(const Range &A);
  Range();
  Range(const char *inFile, const char *outFile);
  ~Range();
  void closeFile();
  void decompress();
  void compress();
  void setInFile(const char *nameFile);
  void setOutFile(const char *nameFile);
};
#endif
